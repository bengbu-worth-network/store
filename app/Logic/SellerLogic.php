<?php

namespace App\Logic;

use App\Library\Logic\BaseSellerLogic;
use App\Library\Requests\Seller\SellerStorePageRequest;

class SellerLogic extends BaseSellerLogic
{
    /**
     * paging
     * @param SellerStorePageRequest $request
     * @return array
     */
    public function paging(SellerStorePageRequest $request): array
    {
        $builder = $this->newQuery()->leftJoin('user', 'user.id', 'seller.user_id')
            ->leftJoin('seller_dm', 'seller_dm.user_id', 'seller.user_id')
            ->leftJoin('seller_tj', 'seller_tj.user_id', 'seller.user_id')->storeId(staff('store.id'));
        if (is_null($request->sortColumn)) $builder->orderByDesc('recommend')->orderByDesc('seller.status')
            ->orderByDesc('seller.id');
        if (!is_null($request->ageLeft)) {
            $builder->where('age', '>=', $request->ageLeft);
            request()->offsetUnset('ageLeft');
        }
        if (!is_null($request->ageRight)) {
            $builder->where('age', '<=', $request->ageRight);
            request()->offsetUnset('ageRight');
        }
        if (!is_null($request->sellerTag)) {
            $builder->whereJsonContains('seller_dm.seller_tag', $request->sellerTag);
            request()->offsetUnset('sellerTag');
        }
        $paginate = helpBuilder($builder->with(['position']), $request)->paginate($request->limit, [
            'seller.user_id',
            'seller.serial_number',
            'avatar_file_id',
            'real_name',
            'name',
            'gender',
            'age',
            'mobile',
            'user.status as user_status',
            'seller.status as seller_status',
            'seller_tag',
            'remark',
            'online',
            'busy',
            'complete_count',
            'project_price',
        ]);
        foreach ($paginate->items() as $v) {
            if (!is_null($v->seller_tag)) $v->seller_tag = json_decode($v->seller_tag, true);
        }
        return paginate($paginate);
    }
}
