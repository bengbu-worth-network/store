<?php

namespace App\Logic;

use App\Library\Logic\BaseProjectLogic;
use App\Library\Requests\Project\StoreProjectPageRequest;

class ProjectLogic extends BaseProjectLogic
{
    /**
     * paging
     * @param StoreProjectPageRequest $request
     * @return array
     */
    public function paging(StoreProjectPageRequest $request): array
    {
        $builder = $this->newQuery()->with('type')->orderBy('sort');
        request()->offsetSet('status', 1);
        return paginate(helpBuilder($builder, $request)->paginate($request->limit, $this->columns()));
    }

    /**
     * paging
     * @return array
     */
    public function alls(): array
    {
        return $this->newQuery()->status(1)->orderBy('sort')->get(['title', 'serial_number', 'price'])->toArray();
    }

    public function columns(): array
    {
        return [
            'serial_number',
            'is_sub',
            'title',
            'file_id',
            'price',
            'duration',
            'tag',
            'explain',
            'feature',
            'main_img_ids',
            'desc_img_ids',
            'required',
        ];
    }
}
