<?php

namespace App\Logic;

use App\Library\Logic\BaseEvaluateLogic;
use App\Library\Requests\Base\BasePageRequest;

class EvaluateLogic extends BaseEvaluateLogic
{
    /**
     * paging
     * @param BasePageRequest $request
     * @return array
     */
    public function paging(BasePageRequest $request): array
    {
        $builder = $this->newQuery()->leftJoin('user as seller_user', 'seller_user.id', 'evaluate.seller_id')
            ->leftJoin('seller', 'seller.user_id', 'seller_user.id')
            ->leftJoin('seller_dm', 'seller_dm.user_id', 'seller.user_id')
            ->leftJoin('user_dm', 'user_dm.user_id', 'evaluate.user_id')
            ->storeId(staff('storeId'))->orderBy('created_at');
        return paginate(helpBuilder($builder, $request)->paginate($request->limit, [
            'evaluate.*',
            'seller_dm.real_name',
            'seller.serial_number as seller_number',
            'seller_user.mobile as seller_mobile',
            'user_dm.nickname',
        ]));
    }
}
