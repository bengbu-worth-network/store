<?php

namespace App\Logic;

use App\Library\Logic\BaseUserLogic;
use App\Library\Requests\Base\BasePageRequest;
use Illuminate\Support\Facades\DB;

class UserLogic extends BaseUserLogic
{
    /**
     * paging
     * @param BasePageRequest $request
     * @param array $stand
     * @return array
     */
    public function paging(BasePageRequest $request, array $stand): array
    {
        $cityCode = StandLogic::query()->whereIn('id', $stand)->pluck('city_code')->toArray();
        $builder = self::query()->leftJoin('mobile_address', 'mobile_address.mobile', 'user.mobile')
            ->leftJoin('user_dm', 'user_dm.user_id', 'user.id')->userTypeId(2)
            ->whereIn('city_code', $cityCode)->orderByDesc('user.id');
        $paginate = helpBuilder($builder, $request)->paginate($request->limit, [
            'status', 'nickname', 'gender', 'city_code', 'auth_status',
            'result->data->province as province',
            'result->data->city as city',
            'result->data->isp as isp',
            'user.created_at',
            'user.serial_number',
            'avatar_file_id',
            DB::raw("concat(left(user.mobile,3),'*****',right(user.mobile,3)) mobile")
        ]);
        return paginate($paginate);
    }
}
