<?php

namespace App\Logic;

use App\Library\Logic\BaseOrderReportLogic;

class OrderReportLogic extends BaseOrderReportLogic
{

    public static function getSellerFaultCount(): int
    {
        return self::query()->leftJoin('seller', 'seller_id', 'seller.user_id')
            ->where('seller.store_id', staff('storeId'))
            ->where('responsible_party', 'seller')->distinct('order_report.seller_id')->count();
    }
}
