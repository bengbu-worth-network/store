<?php

namespace App\Logic;

use App\Library\Logic\BaseWithdrawLogic;
use App\Library\Requests\Withdraw\StoreWithdrawPageRequest;

class WithdrawLogic extends BaseWithdrawLogic
{
    public function paging(StoreWithdrawPageRequest $request): array
    {
        $builder = $this->newQuery()->userId(StoreLogic::valueByScope('id', staff('storeId'), 'userId'));
        return paginate(helpBuilder($builder, $request)->paginate($request->limit,[
            'serial_number',
            'payment_method_id',
            'name',
            'identity',
            'amount',
            'fee',
            'status',
            'status_arrival',
            'ok_reason',
            'fail_reason',
            'file_id',
            'remark',
            'pay_at',
            'audit_at',
            'created_at',
        ]));
    }
}
