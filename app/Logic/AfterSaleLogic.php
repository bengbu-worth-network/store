<?php

namespace App\Logic;

use App\Library\Logic\BaseAfterSaleLogic;

class AfterSaleLogic extends BaseAfterSaleLogic
{
    /**
     *
     * 技师原因发起的售后订单数
     * @return int
     * @Author Sun Wu
     *
     */
    public static function getSellerFaultCount(): int
    {
        return self::query()->leftJoin('seller', 'seller_id', 'seller.user_id')
            ->where('seller.store_id', staff('storeId'))
            ->where('responsible_party', 'seller')->distinct('after_sale.seller_id')->count();
    }
}
