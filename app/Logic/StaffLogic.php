<?php

namespace App\Logic;

use App\Library\Logic\BaseStaffLogic;
use App\Library\Requests\Base\BasePageRequest;

class StaffLogic extends BaseStaffLogic
{
    /**
     * paging
     * @param BasePageRequest $request
     * @return array
     */
    public function paging(BasePageRequest $request): array
    {
        $builder = $this->newQuery()->userId(0)->storeId(staff('storeId'))->orderBy('created_at');
        return paginate(helpBuilder($builder, $request)->paginate($request->limit, $this->columns()));
    }

    /**
     * paging
     * @return array
     */
    public function columns(): array
    {
        return ['id', 'mobile', 'nickname', 'created_at'];
    }
}
