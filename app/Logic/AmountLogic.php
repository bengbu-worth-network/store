<?php

namespace App\Logic;

use App\Library\Logic\BaseAmountLogic;
use App\Library\Requests\Amount\AmountPageRequest;

class AmountLogic extends BaseAmountLogic
{
    public static function todayCommission()
    {
        return self::userId(staff('store.user_id'))
            ->where('pending', 1)
            ->whereBetween('created_at', [now()->startOfDay()->toDateTimeString(), now()->endOfDay()->toDateTimeString()])
            ->sum('amount');
    }

    public function paging(AmountPageRequest $request): array
    {
        $builder = $this->newQuery()->userId(staff('store.user_id'));
        if (!is_null($request->type)) {
            $builder->where('amount', $request->type === 0 ? '>' : '<', 0);
            request()->offsetUnset('type');
        }
        return paginate(helpBuilder($builder->orderByDesc('id'), $request)->paginate($request->limit, [
            'serial_number',
            'title',
            'amount_type',
            'amount',
            'extend',
            'balance',
            'created_at',
        ]));
    }
}
