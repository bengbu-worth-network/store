<?php

namespace App\Http;

use App\Library\Controllers\BaseController;
use App\Library\Requests\UserTag\UserTagAllRequest;
use App\Service\ComService;
use Illuminate\Http\JsonResponse;

class ComController extends BaseController
{
    /**
     * @param ComService $service
     */
    public function __construct(ComService $service)
    {
        $this->service = $service;
    }

    public function userTag(UserTagAllRequest $request): JsonResponse
    {
        return $this->service->userTag($request);
    }
}
