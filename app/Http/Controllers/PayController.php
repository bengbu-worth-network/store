<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Lock;
use App\Library\Requests\Base\SerialNumberRequest;
use App\Service\PayService;
use Illuminate\Http\JsonResponse;

class PayController extends BaseController
{
    /**
     * @param PayService $service
     */
    public function __construct(PayService $service)
    {
        $this->service = $service;
    }

    /**
     * 提现
     * @param SerialNumberRequest $request
     * @return JsonResponse
     */
    public function withdraw(SerialNumberRequest $request): JsonResponse
    {
        return Lock::get($request->serialNumber . __CLASS__ . __FUNCTION__, function () use ($request) {
            return $this->service->withdraw($request->serialNumber);
        });
    }
}
