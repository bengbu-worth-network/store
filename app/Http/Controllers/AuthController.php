<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Login\LoginPasswordRequest;
use App\Library\Requests\Login\UpdatePasswordRequest;
use App\Service\AuthService;
use Exception;
use Illuminate\Http\JsonResponse;

class AuthController extends BaseController
{
    /**
     * @param AuthService $service
     */
    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    /**
     * desc
     *
     * @param LoginPasswordRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function password(LoginPasswordRequest $request): JsonResponse
    {
        return $this->service->password($request);
    }

    /**
     * updatePassword
     * @param UpdatePasswordRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function updatePassword(UpdatePasswordRequest $request): JsonResponse
    {
        return $this->service->updatePwd($request);
    }

    /**
     * logout
     * @return JsonResponse
     * @throws Exception
     */
    public function logout(): JsonResponse
    {
        return $this->service->logout();
    }

    /**
     * info
     * @return JsonResponse
     * @throws Exception
     */
    public function info(): JsonResponse
    {
        return $this->service->info();
    }
}
