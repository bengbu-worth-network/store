<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Store\StoreTjRequest;
use App\Service\StatisticService;
use Illuminate\Http\JsonResponse;

class StatisticController extends BaseController
{
    /**
     * @param StatisticService $service
     */
    public function __construct(StatisticService $service)
    {
        $this->service = $service;
    }

    /**
     * index
     * @param StoreTjRequest $request
     * @return JsonResponse
     */
    public function index(StoreTjRequest $request): JsonResponse
    {
        return $this->service->index($request);
    }
}
