<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Message\MessageListRequest;
use App\Service\MessageService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class MessageController extends BaseController
{
    /**
     * @param MessageService $service
     */
    public function __construct(MessageService $service)
    {
        $this->service = $service;
    }

    /**
     * list
     * @param MessageListRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function list(MessageListRequest $request): JsonResponse
    {
        return $this->service->list($request);
    }
}
