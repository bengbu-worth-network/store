<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Lock;
use App\Library\Requests\Withdraw\StoreWithdrawApplyRequest;
use App\Library\Requests\Withdraw\StoreWithdrawPageRequest;
use App\Service\WithdrawService;
use Exception;
use Illuminate\Http\JsonResponse;

class WithdrawController extends BaseController
{
    /**
     * @param WithdrawService $service
     */
    public function __construct(WithdrawService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param StoreWithdrawPageRequest $request
     * @return JsonResponse =JsonResponse
     */
    public function paging(StoreWithdrawPageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * apply
     * @param StoreWithdrawApplyRequest $request
     * @return JsonResponse
     *
     */
    public function apply(StoreWithdrawApplyRequest $request): JsonResponse
    {
        return Lock::get(USERID . 'withdrawApply', function () use ($request) {
            return $this->service->apply($request);
        });
    }
}
