<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Base\IdRequest;
use App\Library\Requests\SystemConfig\SystemConfigPageRequest;
use App\Service\SystemConfigService;
use Illuminate\Http\JsonResponse;

class SystemConfigController extends BaseController
{
    /**
     * @param SystemConfigService $service
     */
    public function __construct(SystemConfigService $service)
    {
        $this->service = $service;
    }

    /**
     * desc
     *
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return $this->service->getAll();
    }

    /**
     * desc
     *
     * @param SystemConfigPageRequest $request
     * @return JsonResponse
     */
    public function paging(SystemConfigPageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * desc
     *
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function detail(IdRequest $request): JsonResponse
    {
        return $this->service->detail($request);
    }
}
