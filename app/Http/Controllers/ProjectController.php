<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Base\ProjectNumberRequest;
use App\Library\Requests\Project\StoreProjectPageRequest;
use App\Service\ProjectService;
use Illuminate\Http\JsonResponse;

class ProjectController extends BaseController
{
    /**
     * @param ProjectService $service
     */
    public function __construct(ProjectService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param StoreProjectPageRequest $request
     * @return JsonResponse
     */
    public function paging(StoreProjectPageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * all
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        return $this->service->all();
    }

    /**
     * detail
     * @param ProjectNumberRequest $request
     * @return JsonResponse
     */
    public function detail(ProjectNumberRequest $request): JsonResponse
    {
        return $this->service->detail($request);
    }
}
