<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Base\BasePageRequest;
use App\Service\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends BaseController
{
    /**
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }
}
