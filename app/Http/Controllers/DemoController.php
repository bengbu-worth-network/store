<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use Illuminate\Http\JsonResponse;

class DemoController extends BaseController
{
    /**
     * index
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return success();
    }
}
