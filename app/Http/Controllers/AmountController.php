<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Amount\AmountPageRequest;
use App\Library\Requests\StoreAmount\StoreAmountPageRequest;
use App\Service\AmountService;
use Illuminate\Http\JsonResponse;

class AmountController extends BaseController
{
    /**
     * @param AmountService $service
     */
    public function __construct(AmountService $service)
    {
        $this->service = $service;
    }

    public function paging(AmountPageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }
}
