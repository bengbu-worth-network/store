<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Lock;
use App\Library\Requests\Base\OrderNumberRequest;
use App\Library\Requests\Order\OrderCallPagingRequest;
use App\Library\Requests\Order\OrderSellerPagingRequest;
use App\Library\Requests\Order\OrderSendRequest;
use App\Library\Requests\Order\OrderStorePagingRequest;
use App\Service\OrderService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class OrderController extends BaseController
{
    /**
     * @param OrderService $service
     */
    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param OrderStorePagingRequest $request
     * @return JsonResponse
     */
    public function paging(OrderStorePagingRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * detail
     * @param OrderNumberRequest $request
     * @return JsonResponse
     */
    public function detail(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->detail((int)cache('orderNumber:' . $request->serialNumber));
    }

    /**
     * sendPaging
     * @param OrderStorePagingRequest $request
     * @return JsonResponse
     */
    public function sendPaging(OrderStorePagingRequest $request): JsonResponse
    {
        return $this->service->sendPaging($request);
    }

    /**
     * 订单电话拦截
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function telInterceptor(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->telInterceptor($request);
    }

    /**
     * 订单跟踪
     * @param OrderNumberRequest $request
     * @return JsonResponse
     */
    public function tail(OrderNumberRequest $request): JsonResponse
    {
        return Lock::get($request->serialNumber . __CLASS__ . __FUNCTION__, function () use ($request) {
            return $this->service->tail(orderId($request));
        });
    }

    /**
     * 订单通话记录
     * @param OrderCallPagingRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function callRecordPaging(OrderCallPagingRequest $request): JsonResponse
    {
        return $this->service->callRecordPaging($request);
    }

    /**
     * @param OrderSellerPagingRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function sellerPaging(OrderSellerPagingRequest $request): JsonResponse
    {
        return $this->service->sellerPaging($request);
    }

    /**
     * 设置派单状态
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function sendFlag(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->sendFlag($request);
    }

    /**
     * 派单
     * @param OrderSendRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function send(OrderSendRequest $request): JsonResponse
    {
        return $this->service->send($request);
    }

    /**
     * 出发
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function setOut(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->setOut($request);
    }

    /**
     * 到达
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function reach(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->reach($request);
    }

    /**
     * 开始服务
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function serviceStart(OrderNumberRequest $request): JsonResponse
    {
        return $this->service->serviceStart($request);
    }
}
