<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Lock;
use App\Library\Requests\Base\SellerNumberRequest;
use App\Library\Requests\Seller\SellerChooseProjectRequest;
use App\Library\Requests\Seller\SellerResetRequest;
use App\Library\Requests\Seller\SellerStorePageRequest;
use App\Service\SellerService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class SellerController extends BaseController
{
    /**
     * @param SellerService $service
     */
    public function __construct(SellerService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param SellerStorePageRequest $request
     * @return JsonResponse
     */
    public function paging(SellerStorePageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * chooseProject
     * @param SellerChooseProjectRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function chooseProject(SellerChooseProjectRequest $request): JsonResponse
    {
        return $this->service->chooseProject($request);
    }

    /**
     * detail
     * @param SellerNumberRequest $request
     * @return JsonResponse
     */
    public function detail(SellerNumberRequest $request): JsonResponse
    {
        return $this->service->detail($request);
    }

    /**
     * 重置用户信息
     * @param SellerResetRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function reset(SellerResetRequest $request): JsonResponse
    {
        return $this->service->reset($request);
    }
}
