<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Base\BasePageRequest;
use App\Service\EvaluateService;
use Illuminate\Http\JsonResponse;

class EvaluateController extends BaseController
{
    /**
     * @param EvaluateService $service
     */
    public function __construct(EvaluateService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }
}
