<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Library\Requests\Base\BasePageRequest;
use App\Library\Requests\Base\IdRequest;
use App\Library\Requests\Staff\StaffAddRequest;
use App\Library\Requests\Staff\StaffEditRequest;
use App\Service\StaffService;
use Illuminate\Http\JsonResponse;

class StaffController extends BaseController
{
    /**
     * @param StaffService $service
     */
    public function __construct(StaffService $service)
    {
        $this->service = $service;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        return $this->service->paging($request);
    }

    /**
     * detail
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function detail(IdRequest $request): JsonResponse
    {
        return $this->service->detail($request);
    }

    /**
     * add
     * @param StaffAddRequest $request
     * @return JsonResponse
     */
    public function add(StaffAddRequest $request): JsonResponse
    {
        return $this->service->add($request);
    }

    /**
     * edit
     * @param StaffEditRequest $request
     * @return JsonResponse
     */
    public function edit(StaffEditRequest $request): JsonResponse
    {
        return $this->service->edit($request);
    }

    /**
     * delete
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function delete(IdRequest $request): JsonResponse
    {
        return $this->service->delete($request);
    }

    public function myStore(): JsonResponse
    {
        return $this->service->myStore();
    }
}
