<?php

namespace App\Http\Controllers;

use App\Library\Controllers\BaseController;
use App\Service\HomeService;
use Illuminate\Http\JsonResponse;

class HomeController extends BaseController
{
    /**
     * @param HomeService $service
     */
    public function __construct(HomeService $service)
    {
        $this->service = $service;
    }

    /**
     * index
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->service->index();
    }
}
