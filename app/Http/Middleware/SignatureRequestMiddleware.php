<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SignatureRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        //if ($request->header('platform') === 'mp' && prod()) return failMessage('系统维护中');
        if (!prod()) return $next($request);
        else if (redis('hexists', 'ipBlack', $request->ip())) return fail(440);
        $signature = $request->header('signature');
        if (is_null($signature) || !ctype_digit($signature)) return fail(410);
        $signature = decodeId($signature);
        if ($signature < 666666 || $signature > 888888) return fail(410);
        return $next($request);
    }
}
