<?php

namespace App\Service;

use App\Library\Requests\Base\BasePageRequest;
use App\Logic\EvaluateLogic;
use Illuminate\Http\JsonResponse;

class EvaluateService extends StoreBaseService
{
    /**
     * desc
     *
     * @return EvaluateLogic
     */
    private function logic(): EvaluateLogic
    {
        if (empty($this->logic)) $this->logic = new EvaluateLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }
}
