<?php

namespace App\Service;

use App\Library\Requests\Base\SellerNumberRequest;
use App\Library\Requests\Seller\SellerChooseProjectRequest;
use App\Library\Requests\Seller\SellerResetRequest;
use App\Library\Requests\Seller\SellerStorePageRequest;
use App\Logic\SellerLogic;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class SellerService extends StoreBaseService
{
    /**
     * logic
     * @return SellerLogic
     */
    private function logic(): SellerLogic
    {
        if (empty($this->logic)) $this->logic = new SellerLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param SellerStorePageRequest $request
     * @return JsonResponse
     */
    public function paging(SellerStorePageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }

    /**
     * reset
     * @param SellerResetRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function reset(SellerResetRequest $request): JsonResponse
    {
        $order = $this->logic()->findByUserId((int)cache('sellerNumber:' . $request->serialNumber), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * chooseProject
     * @param SellerChooseProjectRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function chooseProject(SellerChooseProjectRequest $request): JsonResponse
    {
        $order = $this->logic()->findByUserId((int)cache('sellerNumber:' . $request->serialNumber), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * detail
     * @param SellerNumberRequest $request
     * @return JsonResponse
     */
    public function detail(SellerNumberRequest $request): JsonResponse
    {
        return success($request->all());
    }
}
