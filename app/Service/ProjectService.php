<?php

namespace App\Service;

use App\Library\Requests\Base\ProjectNumberRequest;
use App\Library\Requests\Project\StoreProjectPageRequest;
use App\Logic\ProjectLogic;
use Illuminate\Http\JsonResponse;

class ProjectService extends StoreBaseService
{
    /**
     * logic
     * @return ProjectLogic
     */
    private function logic(): ProjectLogic
    {
        if (empty($this->logic)) $this->logic = new ProjectLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param StoreProjectPageRequest $request
     * @return JsonResponse
     */
    public function paging(StoreProjectPageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }

    /**
     * all
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        return success($this->logic()->alls());
    }

    /**
     * detail
     * @param ProjectNumberRequest $request
     * @return JsonResponse
     */
    public function detail(ProjectNumberRequest $request): JsonResponse
    {
        $m = $this->logic()->findBySerialnumber($request, array_merge($this->logic()->columns(), ['status']));
        if (is_null($m) || $m->status !== 1) return fail(433);
        else return success($m);
    }
}
