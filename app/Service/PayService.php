<?php

namespace App\Service;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Artisan;

class PayService extends StoreBaseService
{
    /**
     * 退款
     * @param string $serialNumber
     * @return JsonResponse
     */
    public function withdraw(string $serialNumber): JsonResponse
    {
        Artisan::call('command:' . config('app.name') . ucfirst(__FUNCTION__), ['serialNumber' => $serialNumber]);
        return success();
    }
}
