<?php

namespace App\Service;

use App\Library\Requests\Base\BasePageRequest;
use App\Library\Requests\UserTag\UserTagAllRequest;
use App\Logic\EvaluateLogic;
use App\Logic\UserTagLogic;
use Illuminate\Http\JsonResponse;

class ComService extends StoreBaseService
{
    /**
     * userTag
     * @param UserTagAllRequest $request
     * @return JsonResponse
     */
    public function userTag(UserTagAllRequest $request): JsonResponse
    {
        return success(UserTagLogic::query()->type($request->type)->pluck('name', 'id'));
    }
}
