<?php

namespace App\Service;

use App\Library\Requests\Amount\AmountPageRequest;
use App\Logic\AmountLogic;
use Illuminate\Http\JsonResponse;

class AmountService extends StoreBaseService
{
    /**
     * desc
     *
     * @return AmountLogic
     */
    private function logic(): AmountLogic
    {
        if (empty($this->logic)) $this->logic = new AmountLogic;
        return $this->logic;
    }

    /**
     * 账户流水
     * @param AmountPageRequest $request
     * @return JsonResponse
     * @Author Sun Wu
     *
     */
    public function paging(AmountPageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }
}
