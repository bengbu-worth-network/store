<?php

namespace App\Service;

use App\Library\Requests\Base\BasePageRequest;
use App\Logic\StoreLogic;
use App\Logic\UserLogic;
use Illuminate\Http\JsonResponse;

class UserService extends StoreBaseService
{
    /**
     * logic
     * @return UserLogic
     */
    private function logic(): UserLogic
    {
        if (empty($this->logic)) $this->logic = new UserLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        $store = StoreLogic::detail(staff('store_id'));
        if (empty($store->stand)) return failMessage('加盟商还未设置站点');
        else return success($this->logic()->paging($request, $store->stand));
    }
}
