<?php

namespace App\Service;

use App\Library\Requests\Base\OrderNumberRequest;
use App\Library\Requests\Order\OrderCallPagingRequest;
use App\Library\Requests\Order\OrderSellerPagingRequest;
use App\Library\Requests\Order\OrderSendRequest;
use App\Library\Requests\Order\OrderStorePagingRequest;
use App\Logic\OrderLogic;
use App\Logic\OrderRecordingLogic;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class OrderService extends StoreBaseService
{
    /**
     * logic
     * @return OrderLogic
     */
    private function logic(): OrderLogic
    {
        if (empty($this->logic)) $this->logic = new OrderLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param OrderStorePagingRequest $request
     * @return JsonResponse
     */
    public function paging(OrderStorePagingRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }

    /**
     * detail
     * @param int $id
     * @return JsonResponse
     */
    public function detail(int $id): JsonResponse
    {
        $m = $this->logic()->builder(['positionOrder', 'setOutPosition'])->storeId(staff('storeId'))
            ->find($id, $this->logic()->columns());
        if (is_null($m)) return fail(433);
        $this->logic()->incomeDetail($m);
        $this->logic()->incomeDetail($m, 'seller');
        $this->logic()->unsetIncome($m->income);
        $m->recording = OrderRecordingLogic::getByOrderId($m->id);
        unset($m->id);
        return success($m);
    }

    /**
     * sendPaging
     * @param OrderStorePagingRequest $request
     * @return JsonResponse
     */
    public function sendPaging(OrderStorePagingRequest $request): JsonResponse
    {
        return success($this->logic()->sendPaging($request));
    }

    /**
     * 订单电话拦截
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function telInterceptor(OrderNumberRequest $request): JsonResponse
    {
        $m = $this->logic()->findBySerialNumber($request, ['id', 'storeId']);
        if (is_null($m) || $m->store_id !== staff('storeId')) return fail(433);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * 订单跟踪
     * @param int $orderId
     * @return JsonResponse
     */
    public function tail(int $orderId): JsonResponse
    {
        $order = $this->logic()->detail($orderId, ['id', 'storeId', 'staffId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        else if ($order->staff_id > 0) return failMessage('已经有人跟单');
        else if (!in_array($order->status->status, range(2, 5))) return failMessage('订单状态不正确');
        $order->staff_id = USERID;
        $order->save();
        return success();
    }

    /**
     * 订单通话记录
     * @param OrderCallPagingRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function callRecordPaging(OrderCallPagingRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * 派单技师列表
     * @param OrderSellerPagingRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function sellerPaging(OrderSellerPagingRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]), ['storeId' => $order->store_id]);
    }

    /**
     * 设置派单状态
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function sendFlag(OrderNumberRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * 派单
     * @param OrderSendRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function send(OrderSendRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }

    /**
     * 出发
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function setOut(OrderNumberRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]), [
            'remark' => '店铺操作出发',
            'password' => l('base.superToken'),
            'password_confirmation' => l('base.superToken'),
        ]);
    }

    /**
     * 到达
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function reach(OrderNumberRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]), [
            'remark' => '店铺操作到达',
            'password' => l('base.superToken'),
            'password_confirmation' => l('base.superToken'),
        ]);
    }

    /**
     * 开始服务
     * @param OrderNumberRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function serviceStart(OrderNumberRequest $request): JsonResponse
    {
        $order = $this->logic()->detail(orderId($request), ['id', 'storeId']);
        if (is_null($order) || $order->store_id !== staff('storeId')) return fail(439);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]), [
            'remark' => '店铺操作开始服务',
            'password' => l('base.superToken'),
            'password_confirmation' => l('base.superToken'),
        ]);
    }
}
