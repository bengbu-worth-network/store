<?php

namespace App\Service;

use App\Library\Requests\Base\BasePageRequest;
use App\Library\Requests\Base\IdRequest;
use App\Library\Requests\Staff\StaffAddRequest;
use App\Library\Requests\Staff\StaffEditRequest;
use App\Logic\AmountLogic;
use App\Logic\StaffLogic;
use Illuminate\Http\JsonResponse;

class StaffService extends StoreBaseService
{
    /**
     * logic
     * @return StaffLogic
     */
    private function logic(): StaffLogic
    {
        if (empty($this->logic)) $this->logic = new StaffLogic;
        return $this->logic;
    }

    /**
     * paging
     * @param BasePageRequest $request
     * @return JsonResponse
     */
    public function paging(BasePageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }

    /**
     * detail
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function detail(IdRequest $request): JsonResponse
    {
        $m = $this->logic()->detail($request->id, array_merge($this->logic()->columns(), ['storeId']));
        if (is_null($m) || $m->store_id !== staff('storeId')) return fail();
        return success($request->all());
    }

    /**
     * add
     * @param StaffAddRequest $request
     * @return JsonResponse
     */
    public function add(StaffAddRequest $request): JsonResponse
    {
        if ($this->logic()->existsByScope('mobile', $request->mobile)) return fail(444);
        $array = humpToLine(handleRequest($request));
        $array['salt'] = random(32);
        $array['storeId'] = staff('storeId');
        $this->logic()->insertForModel(array_merge($array, [
            'storeId' => staff('storeId'),
            'password' => md5(md5(md5(random())) . $array['salt']),
        ]));
        return success();
    }

    /**
     * edit
     * @param StaffEditRequest $request
     * @return JsonResponse
     */
    public function edit(StaffEditRequest $request): JsonResponse
    {
        $m = $this->logic()->detail($request->id);
        if (is_null($m)) return fail(433);
        else if ($m->store_id !== staff('storeId') || $m->user_id > 0) return fail();
        else if ($this->logic()->existsByScope(['mobile' => $m->mobile, 'id' => ['<>', $m->id]])) return fail(444);
        $array = humpToLine(handleRequest($request));
        if (!is_null($request->password)) {
            $array['salt'] = random(32);
            $array['password'] = md5(md5(md5(random())) . $array['salt']);
        }
        $this->logic()->updateForModel($array, $m);
        $this->fCache($m->id);
        return success();
    }

    /**
     * delete
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function delete(IdRequest $request): JsonResponse
    {
        $m = $this->logic()->detail($request->id);
        if (is_null($m)) return fail(433);
        else if ($m->store_id !== staff('storeId') || $m->user_id > 0) return fail();
        $m->delete();
        $this->fCache($m->id);
        return success();
    }

    public function myStore(): JsonResponse
    {
        $m = $this->logic()->detail(USERID);
        if (is_null($m)) return fail(433);
        return success([
            'licence_file_id' => $m->store->licence_file_id,
            'store_name' => $m->store->name,
            'honor' => 100,
            'id_name' => staff('store.id_name'),
            'identity' => staff('store.identity'),
            'balance' => AmountLogic::balance(staff('store.user_id')),
            'entered' => AmountLogic::entered(staff('store.user_id')),
            'pending' => AmountLogic::pending(staff('store.user_id')),
        ]);
    }
}
