<?php

namespace App\Service;

use App\Library\Requests\Store\StoreTjRequest;
use App\Logic\OrderLogic;
use App\Logic\UserLogic;
use Illuminate\Http\JsonResponse;

class StatisticService extends StoreBaseService
{

    /**
     * index
     * @param StoreTjRequest $request
     * @return JsonResponse
     */
    public function index(StoreTjRequest $request): JsonResponse
    {
        if('2024-03-02 19:59:59' >= now()->toDateString(). ' 20:00:00') {
            $start = now()->toDateString() . ' 20:00:00';
            $end = now()->addDay()->toDateString() . ' 19:59:59';
        }
        else {
            $start = now()->subDay()->toDateString() . ' 20:00:00';
            $end = now()->toDateString() . ' 19:59:59';
        }

        return success(array_merge(OrderLogic::tj($request), [
            'userCount' => UserLogic::query()->whereBetween('created_at', [$start, $end])->count(),
        ]));
    }
}
