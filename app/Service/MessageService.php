<?php

namespace App\Service;

use App\Library\Requests\Message\MessageListRequest;
use App\Logic\MessageLogic;
use App\Logic\OrderLogic;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class MessageService extends StoreBaseService
{
    /**
     * logic
     * @return MessageLogic
     */
    private function logic(): MessageLogic
    {
        if (empty($this->logic)) $this->logic = new MessageLogic;
        return $this->logic;
    }

    /**
     * list
     * @param MessageListRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function list(MessageListRequest $request): JsonResponse
    {
        $key = 'message:' . $request->serialNumber;
        if (!$order = cache($key)) return fail(433);
        else if (!OrderLogic::existsByScope(['id' => $order['id'], 'storeId' => staff('storeId')])) return fail(433);
        return adminCurl(implode('/', [$this->logic()->getTable(), __FUNCTION__]));
    }
}
