<?php

namespace App\Service;

use App\Library\Requests\Base\IdRequest;
use App\Library\Requests\SystemConfig\SystemConfigPageRequest;
use App\Logic\SystemConfigLogic;
use Illuminate\Http\JsonResponse;

class SystemConfigService extends StoreBaseService
{
    /**
     * desc
     *
     * @return SystemConfigLogic
     */
    private function logic(): SystemConfigLogic
    {
        if (empty($this->logic)) $this->logic = new SystemConfigLogic;
        return $this->logic;
    }

    /**
     * Desc:
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return success($this->logic()->getAllCache());
    }

    /**
     * Desc:
     * @param SystemConfigPageRequest $request
     * @return JsonResponse
     */
    public function paging(SystemConfigPageRequest $request): JsonResponse
    {
        return success($this->logic()->paging($request));
    }

    /**
     * Desc:
     * @param IdRequest $request
     * @return JsonResponse
     */
    public function detail(IdRequest $request): JsonResponse
    {
        $item = $this->logic()->detail($request->id);
        if (is_null($item)) return fail(433);
        else return success($item);
    }
}
