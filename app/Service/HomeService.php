<?php

namespace App\Service;

use App\Logic\AfterSaleLogic;
use App\Logic\AmountLogic;
use App\Logic\OrderLogic;
use App\Logic\OrderReportLogic;
use Illuminate\Http\JsonResponse;

class HomeService extends StoreBaseService
{
    /**
     * index
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return success([
            'date' => now()->format('Y年m月d日'),
            'pendingCount' => OrderLogic::indexCount('pending'),
            'servicingCount' => OrderLogic::indexCount('servicing'),
            'completeCount' => OrderLogic::indexCount('complete'),
            'cancelCount' => OrderLogic::indexCount('cancel'),
            'sellerFaultCount' => bcadd(AfterSaleLogic::getSellerFaultCount(), OrderReportLogic::getSellerFaultCount()) * 1,
            'todayCommission' => AmountLogic::todayCommission(),
        ]);
    }
}
