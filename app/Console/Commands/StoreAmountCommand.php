<?php

namespace App\Console\Commands;

use App\Logic\OrderLogic;
use Exception;
use Illuminate\Console\Command;

class StoreAmountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:storeAmount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle(): void
    {
        $list = OrderLogic::query()->whereNotNull('complete_at')->get();
        foreach ($list as $order) {
            //mq('storeAmount', $order);
            /*SellerAmount::updateByScope(['created_at' => $order->complete_at], [
                'objectId' => $order->id,
                'whereIn' => [
                    'amountType' => ['order', 'fill', 'traffic', 'coupon'],
                    'objectType' => ['order', 'settlement'],
                ],
            ]);*/
//            DB::table('seller_amount')->where(['object_id' => $order->id, 'object_type' => 'settlement'])->update([
//                'object_id' => DB::table('settlement')->where('order_id', $order->id)->value('id'),
//            ]);
//            DB::table('order_invite')->where('order_id', $order->id)->update([
//                'created_at' => $order->complete_at,
//            ]);
        }
        $list = OrderLogic::query()->whereNotNull('refund_at')->get();
        foreach ($list as $order) {
            /*SellerAmount::updateByScope(['created_at' => $order->refund_at], [
                'objectId' => $order->id,
                'objectType' => 'order',
                'whereIn' => [
                    'amountType' => ['traffic', 'compensate'],
                ],
            ]);*/
        }
    }

    private function storeIncome(): void
    {
        $list = OrderLogic::query()->whereNotNull('pay_at')->get();
        foreach ($list as $order) {
            if ($order->main_id > 0) continue;
            else if ($order->store->to_join === 1) continue;
            $ratio = $order->income->ratio;
            $sellerDiscountAmount = (float)bcdiv($order->discount_price * $ratio, 100, 2);
            $order->income->seller_discount_amount = $sellerDiscountAmount;
            $order->income->store_discount_allowance = $sellerDiscountAmount;
            $order->income->surplus_amount = (float)$order->order_price - $order->income->seller_amount;
            $surplusAmount = (float)bcdiv($order->income->surplus_amount, 2, 2);
            $order->income->store_amount = $surplusAmount;
            $order->income->store_amount -= $order->income->user_invite;
            $order->income->store_amount -= $order->income->user_invite_up;
            $order->income->store_amount -= $order->income->seller_invite;
            $order->income->store_amount -= $order->income->seller_invite_up;
            $order->income->store_amount -= $sellerDiscountAmount;
            $order->income->store_amount -= $order->income->activity_allowance / 2;
            $order->income->platform_amount = $order->income->surplus_amount - $surplusAmount;
            $order->income->platform_amount -= $order->income->activity_allowance / 2;
            $order->income->platform_discount_allowance = 0;
            $order->income->save();
        }
    }
}
