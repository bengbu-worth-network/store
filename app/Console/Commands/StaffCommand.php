<?php

namespace App\Console\Commands;

use App\Logic\StaffLogic;
use App\Logic\StoreLogic;
use Illuminate\Console\Command;

class StaffCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:staff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $list = StoreLogic::all();
        foreach ($list as $v) {
            $bool = StaffLogic::existsByScope([
                'storeId' => $v->id,
                'userId' => $v->user_id,
            ]);
            if ($bool) continue;
            StaffLogic::insertForModel([
                'mobile' => $v->mobile,
                'storeId' => $v->id,
                'userId' => $v->user_id,
                'password' => $v->user->password,
                'salt' => $v->user->salt,
                'nickname' => random(),
                'created_at' => $v->created_at,
            ]);
        }

    }
}
