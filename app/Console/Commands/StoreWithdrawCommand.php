<?php

namespace App\Console\Commands;

use App\Library\Traits\CommandTrait;
use App\Logic\WithdrawLogic;
use Exception;
use Illuminate\Console\Command;

class StoreWithdrawCommand extends Command
{
    use CommandTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:storeWithdraw {serialNumber}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $this->insertCommand();
        $serialNumber = $this->argument('serialNumber');
        if (empty($serialNumber) || strlen($serialNumber) !== 20) return;
        $withdraw = WithdrawLogic::findBySerialNumber($serialNumber);
        if (is_null($withdraw)) return;
        else $this->handleWithdrawCom($withdraw);
    }
}
