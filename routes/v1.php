<?php

use App\Http\ComController;
use App\Http\Controllers\AmountController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\EvaluateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PayController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StatisticController;
use App\Http\Controllers\SystemConfigController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['signature']], function () {
    Route::post('demo/index', [DemoController::class, 'index'])->name('demo.index');
    //密码登录
    Route::post('login/password', [AuthController::class, 'password'])->name('auth.password');
    //提现
    Route::post('pay/withdraw', [PayController::class, 'withdraw'])->name('pay.withdraw');

    Route::group(['middleware' => ['verifyToken']], function () {
        //系统配置列表
        Route::post('systemConfig/getAll', [SystemConfigController::class, 'getAll'])->name('systemConfig.getAll');
        //用户标签
        Route::post('com/userTag', [ComController::class, 'userTag'])->name('com.userTag');
        //退出登录
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');
        //修改密码
        Route::post('updatePassword', [AuthController::class, 'updatePassword'])->name('updatePassword');
        //info
        Route::post('auth/info', [AuthController::class, 'info'])->name('auth.info');
        //首页
        Route::post('home/index', [HomeController::class, 'index'])->name('home.index');
        //技工列表
        Route::post('seller/paging', [SellerController::class, 'paging'])->name('seller.paging');
        //重置信息
        Route::post('seller/reset', [SellerController::class, 'reset'])->name('seller.reset');
        //选择项目
        Route::post('seller/chooseProject', [SellerController::class, 'chooseProject'])->name('seller.chooseProject');
        //技工详情
        Route::post('seller/detail', [SellerController::class, 'detail'])->name('seller.detail');
        //订单列表
        Route::post('order/paging', [OrderController::class, 'paging'])->name('order.paging');
        //订单详情
        Route::post('order/detail', [OrderController::class, 'detail'])->name('order.detail');
        //派单列表
        Route::post('order/sendPaging', [OrderController::class, 'sendPaging'])->name('order.sendPaging');
        //订单电话拦截
        Route::post('order/telInterceptor', [OrderController::class, 'telInterceptor'])->name('order.telInterceptor');
        //订单跟踪
        Route::post('order/tail', [OrderController::class, 'tail'])->name('order.tail');
        //订单跟踪
        Route::post('order/callRecordPaging', [OrderController::class, 'callRecordPaging'])->name('order.callRecordPaging');
        //派单技师列表
        Route::post('order/sellerPaging', [OrderController::class, 'sellerPaging'])->name('order.sellerPaging');
        //设置派单状态
        Route::post('order/sendFlag', [OrderController::class, 'sendFlag'])->name('order.sendFlag');
        //派单
        Route::post('order/send', [OrderController::class, 'send'])->name('order.send');
        //出发
        Route::post('order/setOut', [OrderController::class, 'setOut'])->name('order.setOut');
        //到达
        Route::post('order/reach', [OrderController::class, 'reach'])->name('order.reach');
        //开始服务
        Route::post('order/serviceStart', [OrderController::class, 'serviceStart'])->name('order.serviceStart');
        //订单监督
        Route::post('message/list', [MessageController::class, 'list'])->name('message.list');
        //项目列表
        Route::post('project/paging', [ProjectController::class, 'paging'])->name('project.paging');
        //所有项目
        Route::post('project/all', [ProjectController::class, 'all'])->name('project.all');
        //项目详情
        Route::post('project/detail', [ProjectController::class, 'detail'])->name('project.detail');
        //员工列表
        Route::post('staff/paging', [StaffController::class, 'paging'])->name('staff.paging');
        //员工列表
        Route::post('staff/paging', [StaffController::class, 'paging'])->name('staff.paging');
        //员工详情
        Route::post('staff/detail', [StaffController::class, 'detail'])->name('staff.detail');
        //添加员工
        Route::post('staff/add', [StaffController::class, 'add'])->name('staff.add');
        //编辑员工
        Route::post('staff/edit', [StaffController::class, 'edit'])->name('staff.edit');
        //删除员工
        Route::post('staff/delete', [StaffController::class, 'delete'])->name('staff.delete');
        //我的商家
        Route::post('staff/myStore', [StaffController::class, 'myStore'])->name('staff.myStore');
        //数据统计
        Route::post('statistic/index', [StatisticController::class, 'index'])->name('statistic.index');
        //评价列表
        Route::post('evaluate/paging', [EvaluateController::class, 'paging'])->name('evaluate.paging');
        //提现记录
        Route::post('withdraw/paging', [WithdrawController::class, 'paging'])->name('withdraw.paging');
        //账户流水
        Route::post('amount/paging', [AmountController::class, 'paging'])->name('amount.paging');
        //提现申请
        Route::post('withdraw/apply', [WithdrawController::class, 'apply'])->name('withdraw.apply');
        //用户列表
        Route::post('user/paging', [UserController::class, 'paging'])->name('user.paging');
    });
});
