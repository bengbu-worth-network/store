<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        table {
            margin-left: 20px;
        }

        table tr {
            height: 30px;
        }

        table tr td {
            width: 200px;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td>描述</td>
        <td>路由</td>
    </tr>
    @foreach($list as $v)
        <tr>
            <td>{{ $v->description }}</td>
            <td>{{ $v->uri }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
