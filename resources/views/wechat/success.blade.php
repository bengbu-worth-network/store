<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>绑定成功</title>
    <style>
        .bind-img-box{
            display: flex;
            justify-content: center;
        }
        .bind-img{
            width: 19.75rem;
            padding: 2.5rem;
        }
        .text{
            line-height: 3.12rem;
            text-align: center;
            font-size: 1.4rem;
        }
    </style>
</head>
<body>
    <div class="bind-img-box">
        <img class="bind-img" src="./img/bind-success.png" alt="">
    </div>
    <p class="text">您已成功绑定微信公众号</p>
</body>
</html>