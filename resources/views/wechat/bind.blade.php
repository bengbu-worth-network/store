<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>绑定公众号</title>
    <style>
        .bind-img-box {
            display: flex;
            justify-content: center;
        }

        .bind-img {
            width: 19.75rem;
            padding: 2.5rem;
        }

        .btn {
            width: 18.75rem;
            height: 3.12rem;
            line-height: 3.12rem;
            text-align: center;
            color: #fff;
            background: url("./img/button-bg.png") no-repeat 100% 100%;
            font-size: 1.2rem;
            margin: 3.5rem auto;
        }

        .form {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .form-item {
            width: 17rem;
            height: 1.4rem;
            display: flex;
            border: 1px solid #616161;
            padding: .6rem;
            margin: .5rem;
            border-radius: .2rem;
        }

        .form-icon {
            margin-right: .5rem;
            width: .8rem;
            height: 1.2rem;
        }

        .send {
            font-size: .75rem;
            width: 4.8rem;
            height: 1.4rem;
            line-height: 1.4rem;
            text-align: center;
            border: .6rem;
            border-radius: .8rem;
            color: #3D75FA;
            border: 1px solid #3D75FA;
        }

        .timer {
            display: none;
            font-size: .7rem;
            width: 4.8rem;
            height: 1.4rem;
            line-height: 1.4rem;
            text-align: center;
            border: .6rem;
            border-radius: .8rem;
            color: #616161;
            border: 1px solid #616161;
        }

        .tip {
            color: red;
            padding: 0px;
            margin: 0px;
            width: 17rem;
            font-size: .8rem;
            margin-top: -.6rem;
            height: .9rem;
        }
    </style>
</head>
<body>
<div class="bind-img-box">
    <img class="bind-img" src="./img/bind.png" alt="">
</div>
<div class="form">
    <div class="form-item">
        <img class="form-icon" src="./img/phone.png" alt="">
        <input type="number" style="border: none;outline: none;" id="phone" placeholder="请输入手机号">
    </div>
    <p class="tip" id="phoneTip"><span style="display: none;">*请输入手机号</span></p>
    <div class="form-item">
        <img class="form-icon" src="./img/pwd.png" alt="">
        <input type="number" style="border: none;outline: none;" id="code" placeholder="请输入验证码">
        <span class="send">发送验证码</span>
        <span class="timer">重新获取<span class="timerNum">60</span>s</span>
    </div>
    <p class="tip" id="codeTip"><span style="display: none;">*请输入验证码</span></p>
</div>
<p class="btn">绑定微信公众号</p>

<script src="./assets/axios.js"></script>
<script>
    // axios.post('https://api.boxtrip.vip/wechatAuth')
    let send = document.querySelector('.send')
    let timer = document.querySelector('.timer')
    let timerNum = document.querySelector('.timerNum')
    let btn = document.querySelector('.btn')
    let phone = document.querySelector('#phone')
    let code = document.querySelector('#code')
    let phoneTip = document.querySelector('#phoneTip span')
    let codeTip = document.querySelector('#codeTip span')
    send.addEventListener('click', function () {
        if (!phone.value) {
            phoneTip.style.display = 'block'
            phoneTip.textContent = '请输入手机号'
            return
        } else {
            phoneTip.style.display = 'none'
        }
        const phoneReg = /^1[3456789]\d{9}$/
        if (phoneReg.test(phone.value)) {
        } else {
            phoneTip.style.display = 'block'
            phoneTip.textContent = '请输入正确的手机号'
            return
        }
        axios.post('https://api.boxtrip.vip/sms/mpSmsCode', {mobile: phone.value}).then((res) => {
            console.log(res);
            let time = 60
            timerNum.textContent = time
            send.style.display = 'none'
            timer.style.display = 'block'
            let timerClear = setInterval(() => {
                time -= 1
                timerNum.textContent = time
                if (time == 0) {
                    clearInterval(timerClear)
                    send.style.display = 'block'
                    timer.style.display = 'none'
                }
            }, 1000)
        })
    })

    btn.addEventListener('click', function () {
        const mobile = phone.value;
        const smsCode = code.value;
        // if(!mobile){
        //     phoneTip.style.display = 'block'
        //     phoneTip.textContent = '*请输入手机号'
        // }else{
        //     phoneTip.style.display = 'none'
        // }
        const phoneReg = /^1[3456789]\d{9}$/
        // if (phoneReg.test(mobile)) {
        // }else{
        //     phoneTip.style.display = 'block'
        //     phoneTip.textContent = '请输入正确的手机号'
        // }
        // if(!smsCode){
        //     codeTip.style.display = 'block'
        //     codeTip.textContent = '*请输入验证码'
        // }else{
        //     codeTip.style.display = 'none'
        // }
        // if(!mobile || !smsCode || !phoneReg.test(mobile)){
        //     return
        // }
        // axios.get(`https://api.boxtrip.vip/wechatAuth?mobile=${mobile}&code=${smsCode}`)
        // .then((res) => {
        //     if(res.code == 200){
        //     }else{
        //         codeTip.style.display = 'block'
        //         codeTip.textContent = '*请输入正确的验证码'
        //     }
        // })
        window.location.href = 'https://api.boxtrip.vip/wechatAuth?mobile=' + mobile + '&code=' + smsCode;
    })
</script>
</body>
</html>

<?php //=config('app.url').'wechatAuth?'?>
