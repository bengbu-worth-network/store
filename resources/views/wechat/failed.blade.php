<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>绑定失败</title>
    <style>
        .bind-img-box{
            display: flex;
            justify-content: center;
        }
        .bind-img{
            width: 19.75rem;
            padding: 2.5rem;
        }
        .text{
            line-height: 3.12rem;
            text-align: center;
            font-size: 1.4rem;
        }
        .btn{
            width: 18.75rem;
            height: 3.12rem;
            line-height: 3.12rem;
            text-align: center;
            color: #fff;
            background: url("./img/button-bg.png") no-repeat 100% 100%;
            font-size: 1.2rem;
            margin: 3.5rem auto;
        }
    </style>
</head>
<body>
    <div class="bind-img-box">
        <img class="bind-img" src="./img/bind-failed.png" alt="">
    </div>
    <p class="text">绑定失败,失败原因:{{ $type }}</p>
    <p class="btn">返回重新绑定</p>

    <script>
        let btn = document.querySelector('.btn')
        btn.addEventListener('click', function(){
            window.location.href = "<?=config('app.url').'wechatBindView'?>";
        })
    </script>
</body>
</html>
