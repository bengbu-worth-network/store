<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        p {
            height: 50px;
        }

        table {
            margin-left: 20px;
        }

        table tr {
            height: 30px;
        }

        table tr td {
            width: 600px;
        }
    </style>
</head>
<body>
@foreach($array as $key => $value)
    <p>{{ $value[0]->TABLE_COMMENT }}:{{ $key }}</p>
    @foreach($value[1] as $v)
        <table>
            <tr>
                <td>{{ $v->Field }}</td>
                <td>{{ $v->Comment }}</td>
            </tr>
        </table>
    @endforeach
    <br/>
@endforeach
</body>
</html>
